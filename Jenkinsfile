pipeline {
    agent none
    options {
        timeout(time: 1, unit: 'HOURS') 
    }

    stages {
        stage('Build and test') {
            agent  {
                docker {
                    image 'maven'
                    args  '-v $HOME/.m2:/root/.m2'
                }
            }

            steps {
                script {
                    def rc = sh script: 'mvn  -f project/images/pom.xml  -Dmaven.test.skip=true clean install', returnStatus: true
                        if(rc > 0 || rc < 0) {
                            currentBuild.result = 'FAILURE'
                        }
                }
                    
                    sh 'mvn -f project/images/pom.xml -Dspring.profiles.active=dev test'              
            }
        }

        stage('Integration test with postman') {
            agent {
                docker {
                    image 'docker/compose:1.25.0-rc4-alpine'
                }
            }

            steps {
                script {
                    def rc = sh script: 'docker-compose -f docker-compose-test.yml run --rm start_dependency_db', returnStatus: true
                     if(rc > 0 || rc < 0) {
                        currentBuild.result = 'FAILURE'
                    }
                 }
                

                script {
                    def rc = sh script: 'docker-compose -f docker-compose-test.yml run --rm start_dependency_app', returnStatus: true
                    if(rc > 0 || rc < 0) {
                        currentBuild.result = 'FAILURE'
                    }
                }
                
                script {
                    def rc = sh script: 'docker-compose -f docker-compose-test.yml up  --force-recreate --build --exit-code-from mytest mytest', returnStatus: true
                    if(rc > 0 || rc < 0) {
                        currentBuild.result = 'FAILURE'
                    }
                }

                sh 'docker-compose -f docker-compose-test.yml down'
                          
            }
        }

        stage ("Clean up") {
            agent any 

            steps {
                sh 'echo cleaning up image no longer need ... '
                sh 'docker rmi  mydb:test${BUILD_NUMBER}'                    
                sh 'docker rmi  maven:latest'  
                sh 'docker rmi  postman/newman:latest'
                sh 'docker rmi  integration:test${BUILD_NUMBER}'
                sh 'echo finished cleaning up image no longer need ... '

            }
        }

        stage("deploy") {
            agent {
                docker {
                    image 'docker/compose:1.25.0-rc4-alpine'
                }
            }

            steps {
                script {
                    if (currentBuild.result != 'FAILURE' || currentBuild.result != 'UNSTABLE') {  
                     
                        def rc2 = sh script: 'docker-compose -f docker-compose-prod.yml up  --build --force-recreate  -d' , returnStatus: true
                        def rc  = sh script: 'docker rmi  myapp:test${BUILD_NUMBER}', returnStatus: true

                        if (rc2 > 0 || rc2 < 0) {
                            sh 'docker-compose -f docker-compose-prod.yml down'
                            sh 'echo DEPLOYING FAIL'
                            currentBuild.result = 'FAILURE'
                        }
    
                    }
                }
            }
        }    
    }
}
