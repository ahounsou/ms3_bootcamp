FROM postgres
LABEL maintainer="ahounsou@ms3-inc.com" 
COPY ./dockerfile/prod/init_user_db.sql  /docker-entrypoint-initdb.d/init-user-db.sql