#This is a  Image is  for an api 
FROM ubuntu
LABEL maintainer="ahounsou@ms3-inc.com"
VOLUME /tmp
RUN  apt-get update

#install java
RUN apt-get -y install default-jdk

#install image I/O packages for loading various image file formats from disk
RUN apt-get -y install libjpeg8-dev libpng-dev
RUN apt-get -y install  libavformat-dev libswscale-dev libdc1394-22-dev
RUN apt-get -y install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev
RUN apt-get -y install libatlas-base-dev
RUN apt-get -y install libfaac-dev libmp3lame-dev libtheora-dev
RUN apt-get -y install libxvidcore-dev libx264-dev
RUN apt-get -y install libopencore-amrnb-dev libopencore-amrwb-dev
RUN apt-get -y install libgphoto2-dev libeigen3-dev libhdf5-dev doxygen x264 v4l-utils
ARG JAR_FILE
ARG DLL_LIB
COPY ${DLL_LIB}  /usr/lib/libopencv_java411.so  
COPY ${JAR_FILE} /usr/src/app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-Dspring.profiles.active=test", "-jar","usr/src/app.jar"]
