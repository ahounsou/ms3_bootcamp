FROM postman/newman

LABEL maintainer="ahounsou@ms3-inc.com"

# RUN npm install -g newman 

ARG INTEGRATION_TEST

COPY ${INTEGRATION_TEST}  /etc/newman/Integration_test.json

WORKDIR /etc/newman

ENTRYPOINT ["newman", "run" , "Integration_test.json"]

