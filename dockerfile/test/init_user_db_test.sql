DROP DATABASE IF EXISTS testdb; 
DROP USER IF EXISTS test;
CREATE user test WITH PASSWORD 'mountainstatesoftware';
CREATE DATABASE testdb OWNER test;