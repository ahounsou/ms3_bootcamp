FROM postgres
LABEL maintainer="ahounsou@ms3-inc.com" 
COPY ./dockerfile/test/init_user_db_test.sql  /docker-entrypoint-initdb.d/init-user-test-db.sql