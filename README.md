# Summary:
 An API written in springboot that make use of opencv to enhance image 
 and store them in a database. 

# Installation Instructions
Requirements:
 - Maven 
 - Docker 
 - Docker-compose

Create a jar file of the Spring-boot api

    mvn -Dmaven.test.skip=true clean package 

Start Database and Wait for connection

    docker-compose -f docker-compose-prod.yml run --rm start_dependency_db'

Start spring-boot-app

    docker-compose -f docker-compose-prod.yml up myapp

Note: A folder name app-data and db-data will be create at /  your filesystem. app-data will contain all the image saved by the api and db-data will contain the database information.

After running the above command, you can connect to springboot by going to localhost:8080

# Deployment
Requirements:
- docker
- docker-compose


# Pipeline Integration
Description:
- Use a docker container of jenkins preferably jenkins/blueocean to run the pipeline.

Requirements:
- docker
- docker-compose

## steps
In the jenkins_docker_script folder, there is a docker-compose file that can be use to  create a jenkins container. But before that we need to run a few command.
 - Create a volume to so the data create by the container can persist
    
        sudo mkdir -p /volume1/docker/jenkins

 - Grant container user rights

        sudo chown -R 1000:1000 /volume1/docker/jenkins

 - Grant right to  "host" docker 

        sudo chown -R 1000:1000 /var/run/docker.sock
 - Create jenkins container 
    
        docker-compose -f docker-compose-jenkins.yaml up

More info at this link:  https://keestalkstech.com/2018/03/setup-jenkins-blue-ocean-using-docker-on-synology-nas/



    


    








