import requests
import base64
import json
from PIL import Image


port = "8080"

def basic_auth(username, password):
    return str(base64.b64encode(bytes(username + ':' + password,  'utf-8')), 'utf-8')


def test_connection():
    url = f'http://localhost:{port}/'
    result = requests.get(url= url)
    print(result)


def test_post_images():
    url = f"http://localhost:{port}/api/v1/images"
    headers = {}
    headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) \
    AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 \
    Safari/537.17"

    headers['Authorization'] = f"Basic {basic_auth('axel', 'pass')}"

    with open("./test_img/panda.jpg", "rb") as img:
        encoded_base64 = base64.b64encode(img.read())
        encoded_string = str(encoded_base64, 'utf-8')

    # with open("temp.text", "wb") as temp:
    #     temp.write(str(encoded_base64, "utf-8"))

    data = {"imageData": encoded_string,
            "filters": ["black_and_white"],
            "imageExt": ".jpg"}

    result = requests.post(url=url,  headers=headers, json=data)

    result_json = result.json()

    print("Test post image")
    print(f" username: axel role: user  response code: {result.status_code}")
    print(f"image data with id: {result_json['id']} returned")
    print("\n\n")

    with open("imageToSave01.jpg", "wb") as fh:
        fh.write(base64.decodebytes(bytes(result_json['imageData'], 'utf-8')))


def test_get_filters():
    headers = {}
    headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) \
    AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 \
    Safari/537.17"

    # headers['Authorization'] = f"Basic {basic_auth('axel', 'pass')}"

    url = f"http://localhost:{port}/api/v1/filters"

    result = requests.get(url=url , headers=headers)

    print("Test get filters")
    print(f" no user  response code: {result.status_code}")
    print (json.dumps(result.json(), indent=4))
    print("\n\n")

def test_get_images():
    headers = {}
    headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) \
    AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 \
    Safari/537.17"

    headers['Authorization'] = f"Basic {basic_auth('axel', 'pass')}"

    url = f"http://localhost:{port}/api/v1/images"

    result = requests.get(url=url , headers=headers)
    
    print("Test get images")
    print(f" username: axel role: admin  response code: {result.status_code}")
    print (json.dumps(result.json(), indent=4))
    print("\n\n")

    return result.json()


def test_get_images_by_id(id):
    headers = {}
    headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) \
    AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 \
    Safari/537.17"

    headers['Authorization'] = f"Basic {basic_auth('axel', 'pass')}"

    url = f"http://localhost:{port}/api/v1/images/{id}" 


    result = requests.get(url=url , headers=headers)

    print("test get image by id")
    print(f"username: axel  role: user  image_id {id} response code: {result.status_code}")
    print("\n\n")

    with open("imageToSave02.jpg", "wb") as fh:
        fh.write(base64.decodebytes(bytes(result.text, 'utf-8')))


def test_register():
    headers = {}
    headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) \
    AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 \
    Safari/537.17"

    headers['Authorization'] = f"Basic {basic_auth('johndoe', 'johndoe')}"
    url = f"http://localhost:{port}/api/v1/register"

    params = {"username": "axel",
              "password": "pass",
              "dob": "12/01/10",
              "email": "axel@company.com"}

    result = requests.post(url=url, headers=headers, json=params)

    print("test register")
    print(f"username: johndoe role: admin  response code: {result.status_code}")
    print (json.dumps(result.json(), indent=4))
    print("\n\n")


def test_register_fail():
    headers = {}
    headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) \
    AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 \
    Safari/537.17"

    headers['Authorization'] = f"Basic {basic_auth('axeldoe', 'axeldoe')}"
    url = f"http://localhost:{port}/api/v1/register"

    params = {"username": "axel",
              "password": "pass",
              "dob": "12/01/10",
              "email": "axel@company.com"}

    result = requests.post(url=url, headers=headers, json=params)

    print("test register fail")
    print(f"username: axeldoe role: user  response code: {result.status_code}")
    print (json.dumps(result.json(), indent=4))
    print("\n\n")


def test_put_images(id, filters, path, imageExt):
    headers = {}
    headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) \
    AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 \
    Safari/537.17"

    headers['Authorization'] = f"Basic {basic_auth('axel', 'pass')}"

    url = f"http://localhost:{port}/api/v1/images/{id}" 

    params = {"description": "this is done by put", "filters": filters, "imageUrl": path , 
    "imageExt": imageExt, "imageData": "This is for testing"}

    result = requests.put (url=url, headers=headers, json=params)
    
    print("test put image by id")
    print(f"username: axel  role:user  image_id {id} response code: {result.status_code}")
    print("\n\n")


def test_patch_image(id, filters, path, imageExt):
    headers = {}
    headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) \
    AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 \
    Safari/537.17"

    headers['Authorization'] = f"Basic {basic_auth('axel', 'pass')}"

    url = f"http://localhost:{port}/api/v1/images/{id}" 

    params = {"description": "this is not done by patch "}

    result = requests.patch(url=url, headers=headers, json=params)
    
    print("test patch image by id")
    print(f"username: axel  role:user  image_id {id} response code: {result.status_code}")
    print("\n\n")

def test_delete_image(id):
    headers = {}
    headers['User-Agent'] = "Mozilla/5.0 (X11; Linux i686) \
    AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 \
    Safari/537.17"

    headers['Authorization'] = f"Basic {basic_auth('axel', 'pass')}"

    url = f"http://localhost:{port}/api/v1/images/{id}" 

    result = requests.delete(url=url, headers=headers,)
    
    print("test delete image by id")
    print(f"username: axel  role:user  image_id {id} response code: {result.status_code}")
    print("\n\n")


if __name__ == '__main__':
    test_register_fail()
    test_register()
    test_post_images()
    test_get_filters()
    result = test_get_images()

    for imageData in result:
        test_get_images_by_id(imageData['id'])
        test_put_images(imageData['id'], imageData['filters'], imageData['imageUrl'], imageData['imageExt'])
        test_get_images()
        test_patch_image(imageData['id'], imageData['filters'], imageData['imageUrl'], imageData['imageExt'])
        test_get_images()
        test_delete_image(imageData['id'])
        test_get_images()
        break

