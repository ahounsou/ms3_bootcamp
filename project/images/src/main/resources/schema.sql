
drop table if exists users cascade;
drop table if exists roles;
drop table if exists images;

create table if not exists users(
 id serial primary key,
 username  char(50) not null unique,
 password  char(60) not null,
 dob      char(50) ,
 enabled  boolean default true,
 first_name char(100),
 last_name char(100),
 email char(100) not null unique
);

create table if not exists roles(
 id serial primary key,
 username  char(50) not null,
 role char(10) not null
);


create table if not exists images(
id serial,
image_path char(100),
image_data  text,
description text,
ext char(10),
filters bytea not null,
user_id int references users (id) on delete cascade
);