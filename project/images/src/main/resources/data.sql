insert into users (username, password, first_name, last_name, email)
values (
        'johndoe',
        '$2a$09$ak9egHsLN/tvId3cuY45/.Ne6qBVu09jqzRjfwtIDNRJuDEzJIzUe',
        'john',
        'doe',
        'johndoe@company.com');


insert into users (username, password, first_name, last_name, email)
values (
        'axeldoe',
        '$2a$09$Hs6VzYRR/2AACioiK2BCbe8KHAlYAQv.nLGRFy0teB5SYMz6X4Z3O',
        'axel',
        'doe',
        'axeldoe@company.com');

insert into roles(username, role)
values(
        'johndoe',
        'ROLE_ADMIN');

insert into roles(username, role)
values(
    'axeldoe',
    'ROLE_USER');

