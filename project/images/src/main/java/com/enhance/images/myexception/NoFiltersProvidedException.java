package com.enhance.images.myexception;

public class NoFiltersProvidedException extends  Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1214365931424488082L;

    public NoFiltersProvidedException() {
        super();
    }
    public NoFiltersProvidedException(String message) { super(message); }
    public NoFiltersProvidedException(String message, Throwable cause) { super(message, cause); }
    public NoFiltersProvidedException(Throwable cause) { super(cause); }
}
