package com.enhance.images.service;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class FilterServiceImpl implements  FilterService {

    private  Map<String,String> filtersList;
    private Map<String, String> filtersToClass ;


    public  FilterServiceImpl () {
        setUpFiltersMap();
        setUpFiltersList();
    }


    private  void setUpFiltersMap() {
        this.filtersToClass =  Collections.unmodifiableMap(
                new HashMap<String, String>() {
                    /**
                    *
                    */
                    private static final long serialVersionUID = 5911360725850864466L;

                    {
                    put("black_and_white", "BlackAndWhite");
                    put("grayify", "Grayify");

                }});
    }

    private  void setUpFiltersList() {

        this.filtersList =  Collections.unmodifiableMap(
                new HashMap<String, String>() {
                    /**
                    *
                    */
                    private static final long serialVersionUID = 6982719575854498786L;

                    {
                    put("black_and_white", "transform an image to Black and white");
                    put("grayify", "transform an image to gray");

                }});
    }

    @Override
    public Map<String, String> getFiltersList() {
        return filtersList;
    }

    @Override
    public String convertFilterToClass(String filterName) {
            return filtersToClass.get(filterName);
    }
}
