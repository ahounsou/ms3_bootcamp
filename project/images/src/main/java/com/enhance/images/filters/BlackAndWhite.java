package com.enhance.images.filters;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class BlackAndWhite implements Filters {

    @Override
    public Mat applyFilter(Mat source) {
        
        Mat destination1 = new Mat();
        Mat destination2 = new Mat(); 
        Imgproc.cvtColor(source, destination1, Imgproc.COLOR_RGB2GRAY); 

        Imgproc.threshold(destination1, destination2, 123, 255, Imgproc.THRESH_OTSU);

        return destination2;

    }

}