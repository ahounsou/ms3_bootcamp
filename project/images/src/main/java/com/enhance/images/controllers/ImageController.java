package com.enhance.images.controllers;

import com.enhance.images.models.Image;
import com.enhance.images.myexception.NoFiltersProvidedException;
import com.enhance.images.myexception.NoImageDataException;
import com.enhance.images.myexception.NoImageExtException;
import com.enhance.images.repositories.ImageRepository;
import com.enhance.images.service.ImageService;
import com.enhance.images.xlib.ImageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;



@RestController
@EnableWebSecurity
@CrossOrigin(origins = "http://localhost:9000")
public class ImageController {

    @Autowired
    private ImageService imageService;


  /*get the id of all image*/
    @RequestMapping(value = "/api/v1/images", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Image>> Images() {

        List<Image> toret = imageService.imageGet();

        return ResponseEntity.status(HttpStatus.OK).body(toret);
    }


    /**
     * Apply filter to the image sent by the user and  send the image back as a base64 encoded string
     */
    @RequestMapping(value = "/api/v1/images", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ImageData> enhancesPost(@RequestParam(value = "url", defaultValue = "") String url,
                                                  @RequestBody Image image) {

        ImageData data1 = null;

        try {
            if (url.isEmpty()) {
                data1 = imageService.imagePost(image);
            } else {
                data1 = imageService.imagePostFromUrl(image, url);
            }

            return ResponseEntity.status(HttpStatus.CREATED).body(data1);

        } catch (NoClassDefFoundError e) {
            data1 = new ImageData("One of the filter provided is incorrect.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data1);
        } catch (NoImageDataException e) {
            data1 = new ImageData("Image data not provided.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data1);
        } catch (NoFiltersProvidedException e) {
            data1 = new ImageData("Filters not provided.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data1);
        } catch (NoImageExtException e) {
            data1 = new ImageData("Image ext not provided.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data1);
        } catch (IOException e) {
            data1 = new ImageData("Image ext not found.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(data1);
        }
    }


    /* Get image data by id*/
    @RequestMapping(value = "/api/v1/images/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> ImagesParam(@PathVariable(value = "id") long id) {

        try {
            String strImage = imageService.imageGet(id);

            return ResponseEntity.status(HttpStatus.OK).body(strImage);

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("You are not authorize to view this image");
        }
    }

    /* Modify all column of image by id*/
    @RequestMapping(value = "/api/v1/images/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Image> ImagesPut(@PathVariable(value = "id") long id, @RequestBody Image newImage) {

        try {
            Image image =  imageService.imagePut(newImage, id);
            return ResponseEntity.status(HttpStatus.OK).body(image);

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }


    /* Modify  column of image by id*/
    @RequestMapping(value = "/api/v1/images/{id}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Image> ImagesPatch(@PathVariable(value = "id") long id, @RequestBody Image newImage) {
        try {
             Image image = imageService.imagePatch(newImage, id);
            return ResponseEntity.status(HttpStatus.OK).body(image);

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    /*Delete image by id*/
    @RequestMapping(value = "/api/v1/images/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> ImagesDelete(@PathVariable(value = "id") long id) {

        try{
            imageService.imageDelete(id);
            return ResponseEntity.status(HttpStatus.OK).body("Successful");

        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("You are not authorize to delete this image");

        }
    }

}
