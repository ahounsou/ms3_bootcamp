package com.enhance.images.service;

import java.util.Map;

public interface FilterService {

    public Map<String, String> getFiltersList();
    public String convertFilterToClass(String filterName);

}
