package com.enhance.images.service;

import com.enhance.images.models.User;
import com.enhance.images.models.UserRole;
import com.enhance.images.repositories.UserRepository;
import com.enhance.images.repositories.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImp implements  UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;



    @Override
    public User findByUsername(String username) {

        return userRepository.findByUsername(username);
    }

    @Override
    public User findByEmail(String email) {

        return userRepository.findByEmail(email);
    }

    @Override
    public long createUser(User user) {

        return save(user);
    }

    @Override
    public long save(User user) {

        UserRole userRole = new UserRole();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRole.setUsername(user.getUsername());
        userRole.setRole("ROLE_USER");

        userRoleRepository.save(userRole);
        return userRepository.save(user).getId();

    }
}
