package com.enhance.images.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class MyBasicSecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    private  DataSource dataSource;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {

        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("select username, password, enabled from users "
                        + "where username = ?")
                .authoritiesByUsernameQuery("select username, role  from roles "
                        + "where username = ?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http    
            .cors()     
            .and()
            .csrf()
            .disable()
            .httpBasic()
            .and()
            .authorizeRequests()
            .antMatchers("/api/v1/register")
            .hasRole("ADMIN")
            .and()
            .authorizeRequests()
            .antMatchers("/api/v1/filters")
            .permitAll()
            .and()
            .authorizeRequests()
            .anyRequest()
            .authenticated();

//        http.csrf().disable()
//                .authorizeRequests()
//                .antMatchers("/api/v1/register").hasRole("ADMIN")
//                .and().authorizeRequests().anyRequest().hasAnyRole().and().httpBasic();
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(9);
    }

}