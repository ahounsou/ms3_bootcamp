package com.enhance.images.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Objects;

import javax.persistence.*;

/***
 * Image Models
 */
@Entity
@Table(name = "images")
public class Image {
    /*************************************************Table column name***********************************************/

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "image_path", nullable = true)
    private String imageUrl;

    @Column(name = "image_data", nullable = true)
    private String imageData;

    @Column(name = "description", nullable = true)
    private String description;

    @Column(name = "ext", nullable = true)
    private String imageExt;


    @Column(name = "filters", nullable = true)
    private  ArrayList<String> filters;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    /*****************************************************************************************************************/
    public Image() {}

    /**
     * @param id
     * @param imageUrl
     * @param imageData
     * @param  description
     * @param  filters array
     */
    public Image(long id, String imageUrl, String imageData, String description, ArrayList<String> filters) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.imageData = imageData;
        this.description = description;
        this.filters = filters;

    }


    public String getImageExt() {
        return imageExt;
    }

    public void setImageExt(String imageExt) {
        this.imageExt = imageExt;
    }

    public void setFilters(ArrayList<String> filters) {
        this.filters = filters;
    }

    public ArrayList<String> getFilters() {
        return filters;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @JsonProperty
    public String getImageUrl() {
        return imageUrl;
    }


    @JsonIgnore
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @JsonIgnore
    public String getImageData() {
        return imageData;
    }

    @JsonProperty
    public void setImageData(String imageData) {
        this.imageData = imageData;
    }


    @Override
    public String toString() {
        return "Images{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", filters=" + filters +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return id == image.id &&
                Objects.equals(imageUrl, image.imageUrl) &&
                Objects.equals(imageData, image.imageData) &&
                Objects.equals(description, image.description) &&
                Objects.equals(filters, image.filters) &&
                Objects.equals(user, image.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, imageUrl, imageData, description, filters, user);
    }


}
