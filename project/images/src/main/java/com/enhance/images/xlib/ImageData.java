package com.enhance.images.xlib;

public class ImageData {

    private String imageData;
    private  long id = -1;

    @Override
    public String toString() {
        return "ImageData{" +
                "imageData='" + imageData + '\'' +
                ", id=" + id +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ImageData(String imageData) {
        this.imageData = imageData;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }
}
