package com.enhance.images.controllers;

import com.enhance.images.service.FilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:9000")
public class FiltersController {

    @Autowired
    private FilterService filterService;

    /** Return the list of filter supported by the api*/
   
    @RequestMapping(value = "/api/v1/filters", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
     public ResponseEntity<Map<String,String>> listFilters() {

//        ListFilters filters = new ListFilters();

        return ResponseEntity.status(HttpStatus.OK).body(filterService.getFiltersList());
    }
}
