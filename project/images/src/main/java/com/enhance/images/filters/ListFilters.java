package com.enhance.images.filters;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ListFilters {

    private  Map<String,String> filtersList;
    private Map<String, String> filtersToClass ;


    public ListFilters() {
      setUpFiltersMap();
      setUpFiltersList();
    }


    private  void setUpFiltersMap() {
        this.filtersToClass =  Collections.unmodifiableMap(
                new HashMap<String, String>() {
                    /**
                    *
                    */
                    private static final long serialVersionUID = 5473054959805501890L;

                    {
                    put("black_and_white", "BlackAndWhite");
                    put("grayify", "Grayify");

                }});
    }

    private  void setUpFiltersList() {

        this.filtersList =  Collections.unmodifiableMap(
                new HashMap<String, String>() {
                    /**
                    *
                    */
                    private static final long serialVersionUID = 5627728362331255156L;

                    {
                        put("black_and_white", "transform an image to Black and white");
                        put("grayify", "transform an image to gray");

                    }});
    }



    public Map<String, String> getFiltersList() {
        return filtersList;
    }

/**@param filterName
 * @return  string
 * return the name of thr class associated with filter name
 * */
    public String convertFilterToClass(String filterName) {
        return filtersToClass.get(filterName);
    }
}
