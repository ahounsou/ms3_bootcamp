package com.enhance.images.repositories;

import com.enhance.images.models.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository  extends JpaRepository<UserRole, Long> {
    UserRole findByUsername(String username);
}
