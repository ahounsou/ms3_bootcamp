package com.enhance.images.myexception;

public class NoImageExtException extends  Exception {

        /**
        *
        */
        private static final long serialVersionUID = -6683974274175644361L;

        public NoImageExtException() {
                super();
        }
        public NoImageExtException(String message) { super(message); }
        public NoImageExtException(String message, Throwable cause) { super(message, cause); }
        public NoImageExtException(Throwable cause) { super(cause); }

}
