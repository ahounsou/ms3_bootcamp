package com.enhance.images.xlib;

import org.opencv.core.Mat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * Dynamically load class and invoke method
 * that take one Mat paramater
 */
public class JavaClassLoader extends ClassLoader {

    public Object invokeClassMethod(String classBinName, String methodName, Mat param){

        Object result = null;
        try {

            // Create a new JavaClassLoader
            ClassLoader classLoader = this.getClass().getClassLoader();

            // Load the target class using its binary name
            Class loadedMyClass = classLoader.loadClass(classBinName);

            System.out.println("Loaded class name: " + loadedMyClass.getName());

            // Create a new instance from the loaded class
            Constructor constructor = loadedMyClass.getConstructor();
            Object myClassObject = constructor.newInstance();

            // Getting the target method from the loaded class and invoke it using its name
            Method method = loadedMyClass.getMethod(methodName, Mat.class);
            System.out.println("Invoked method name: " + method.getName());
           result =   method.invoke(myClassObject, param);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
         return  result;
    }
}
