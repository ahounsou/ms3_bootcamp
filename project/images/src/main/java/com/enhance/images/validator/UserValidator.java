package com.enhance.images.validator;

import com.enhance.images.xlib.CustomResponseMessage;
import com.enhance.images.models.User;

public class UserValidator {

    private User user;
    private CustomResponseMessage message;
    private Boolean valid = true;
    private String field;


    public UserValidator(User user) {
        this.user = user;
    }

    /**
     * @throws NoSuchFieldException
     * @return ErrorMessage
     * */
    public void validate() throws NoSuchFieldException {
        if (this.valid) {
            if (user.getEmail() == null) {
                this.valid = false;
                field = "email";
            }
        }

        if (this.valid) {
            if (user.getUsername() == null) {
                this.valid = false;
                field = "username";
            }
        }
        if (this.valid) {
            if (user.getPassword() == null)
            {
                this.valid = false;
                field = "password";
            }
        }

        if(valid == false) {
            throw new NoSuchFieldException();
        }
    }

    public  CustomResponseMessage customResponseMessage( String customMessage) {
        if (valid && customMessage == null) {
            this.message = new CustomResponseMessage( user + " has been created.");
        } else if(valid && customMessage != null) {
            this.message = new CustomResponseMessage(customMessage);
        } else {
            this.message = new CustomResponseMessage(String.format("%s is null ", field));
        }
        return  message;
    }
}
