package com.enhance.images;

import org.opencv.core.Core;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ImagesApplication {

	public static void main(String[] args) {

		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		SpringApplication.run(ImagesApplication.class, args);
	}

//test connection
//	@RequestMapping(value = "/")
//	public String hello() {
//		return  "Hello World";
//	}

}
