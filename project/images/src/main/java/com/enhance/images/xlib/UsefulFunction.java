package com.enhance.images.xlib;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Random;


public class UsefulFunction {

    /**
     * 
     * @param imageUrl
     * @return BufferedImage
     * @throws IOException
     */
    public static BufferedImage readFromUrl(String imageUrl) throws IOException {

        URL url = new URL(imageUrl);
        BufferedImage image = ImageIO.read(url);
        return image;
    }

    /**
     * 
     * @param matrix
     * @param extention
     * @return BufferedImage type
     * @throws IOException
     */
    public static BufferedImage Mat2BufferedImage(Mat matrix, String extention) throws IOException {
        MatOfByte mob = new MatOfByte();
        Imgcodecs.imencode(extention, matrix, mob);
        return ImageIO.read(new ByteArrayInputStream(mob.toArray()));
    }

    /**
     * 
     * @param image
     * @param formatName
     * @return Mat image
     * @throws IOException
     */
    public static Mat bufferedImage2Mat(BufferedImage image, String formatName) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(image, formatName, byteArrayOutputStream);
        byteArrayOutputStream.flush();
        return Imgcodecs.imdecode(new MatOfByte(byteArrayOutputStream.toByteArray()), Imgcodecs.IMREAD_UNCHANGED);
    }


    public  static  byte[] bufferedImageToByte(BufferedImage bufferedImage, String format) throws IOException {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        ImageIO.write(bufferedImage, format.strip(), bos);
        byte[] imageBytes = bos.toByteArray();

        return  imageBytes;

    }

    /**
     * @param bytes
     * @return Mat
     */
    public static Mat byteToMat(byte[] bytes) {
        Mat mat = Imgcodecs.imdecode(new MatOfByte(bytes), Imgcodecs.IMREAD_UNCHANGED);
        return  mat;
    }

    /**
     * @param mat
     * @return bytes
     */
    public static byte[] matToByte(Mat mat, String ext) {

        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(ext, mat, matOfByte);
        return  matOfByte.toArray() ;
    }

    /**
     * @param file
     * @return file format
     */
    public static String getFileFormat(String file) {

        return file.substring(file.lastIndexOf(".") + 1).toLowerCase();
    }


    /**
     * @param file
     * @return file format
     */
    public static String getFileExt(String file) {

        return file.substring(file.lastIndexOf(".")).toLowerCase();
    }

    /**
     * @param data
     * @return base 64 encoded string
     */
    public static String byteToBase64(byte[] data) {

        return Base64.getEncoder().encodeToString(data);
    }

    /**
     * @param data
     * @return byte
     */
    public static byte[] base64ToByte(String data) {

        return Base64.getDecoder().decode(data.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * @param  imageName
     * @return  image
     * @throws IOException
     * */
    public static  BufferedImage readImage (String imageName) throws IOException {
        File file = new File(imageName.strip());
  

        BufferedImage  image =  ImageIO.read(file);

        return image;
    }


    /**
     @param  imageName
     @return  boolean
     */
    public static  boolean deleteImage (String imageName) {
        File file = new File(imageName.strip());

        return file.delete();
    }


/**
 * @param img2
 * @param  ext
 * @param folderName
 * @return path to image */
    public static String saveImage(Mat img2, String ext, String folderName)
    {

        String randString = generateString();
        String imageName = randString + ext;
        String pathToImage = "." + File.separator + "media" + File.separator + folderName.strip() + File.separator + imageName;

        File file = new File(pathToImage);
        file.getParentFile().mkdirs();
        Imgcodecs.imwrite(pathToImage,img2);

        return  pathToImage;
    }

    private static String generateString(){

        Random r = new Random();
        StringBuilder randString = new StringBuilder();

        String alphabet = "012346789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
        for (int i = 0; i < 10; i++) {
        randString.append(alphabet.charAt(r.nextInt(alphabet.length())));
        }

        return  randString.toString();
    }
}