package com.enhance.images.xlib;

public class CustomResponseMessage {

    private String message;


    public CustomResponseMessage() {}

    public void setMessage(String message) {
        this.message = message;
    }

    public CustomResponseMessage(String message) {
        this.message = message;

    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "CustomResponseMessage{" +
                "message='" + message + '\'' +
                '}';
    }
}
