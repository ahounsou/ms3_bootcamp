package com.enhance.images.filters;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class Grayify implements Filters {

    @Override
    public Mat applyFilter(Mat source) {
        Mat destination = new Mat(); 
        Imgproc.cvtColor(source, destination, Imgproc.COLOR_RGB2GRAY);
        return destination;
    }
    
} 