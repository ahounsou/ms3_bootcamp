package com.enhance.images.myexception;

public class NoImageDataException extends  Exception {
    /**
     *
     */
    private static final long serialVersionUID = 8184747528124721145L;

    public NoImageDataException() {
        super();
    }
    public NoImageDataException(String message) { super(message); }
    public NoImageDataException(String message, Throwable cause) { super(message, cause); }
    public NoImageDataException(Throwable cause) { super(cause); }
}
