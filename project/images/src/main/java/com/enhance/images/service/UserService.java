package com.enhance.images.service;

import com.enhance.images.models.User;

public interface UserService {
    long save(User user);

    User findByUsername(String username);
    User findByEmail(String email);
    long createUser(User user);

}