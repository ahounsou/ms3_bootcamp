package com.enhance.images.controllers;

import com.enhance.images.models.User;
import com.enhance.images.service.UserService;
import com.enhance.images.xlib.CustomResponseMessage;
import com.enhance.images.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@EnableWebSecurity
@CrossOrigin(origins = "http://localhost:9000")
public class UsersController {

    @Autowired
    private UserService userService;

    private UserValidator userValidator;

    /** create a new user and add it to the user database*/
    @RequestMapping(value = "/api/v1/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomResponseMessage> createUser(@Valid @RequestBody User user) {

        try {
            userValidator = new UserValidator(user);
            userValidator.validate();
            userService.createUser(user);
            return ResponseEntity.status(HttpStatus.CREATED).body(userValidator.customResponseMessage(null));
        } catch ( DataIntegrityViolationException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT ).body(userValidator.
                    customResponseMessage(e.getMostSpecificCause().getMessage().split("\n")[1]));
        } catch (NoSuchFieldException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST ).body(userValidator.customResponseMessage(null));
        }
    }
}
