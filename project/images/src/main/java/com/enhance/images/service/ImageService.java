package com.enhance.images.service;

import com.enhance.images.models.Image;
import com.enhance.images.myexception.NoFiltersProvidedException;
import com.enhance.images.myexception.NoImageDataException;
import com.enhance.images.myexception.NoImageExtException;
import com.enhance.images.xlib.ImageData;

import java.io.IOException;
import java.util.List;

public interface ImageService {

    long save(Image image);
    List<Image> imageGet();
    ImageData imagePost(Image image) throws NoImageDataException, NoFiltersProvidedException, NoImageExtException, IOException;
    ImageData imagePostFromUrl(Image image, String imageUrl) throws NoImageDataException, NoFiltersProvidedException, NoImageExtException, IOException;
    Image imagePut(Image image, long id) throws IOException;
    Image imagePatch(Image image, long id) throws IOException;
    long imageDelete(long id) throws IOException;
    String imageGet(long id) throws IOException;

}
