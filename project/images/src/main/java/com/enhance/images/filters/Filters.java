package com.enhance.images.filters;

import org.opencv.core.Mat;

public interface Filters {

    Mat applyFilter(Mat imageMat);

}