package com.enhance.images.service;

import com.enhance.images.filters.ListFilters;
import com.enhance.images.models.Image;
import com.enhance.images.myexception.NoFiltersProvidedException;
import com.enhance.images.myexception.NoImageDataException;
import com.enhance.images.myexception.NoImageExtException;
import com.enhance.images.repositories.ImageRepository;
import com.enhance.images.repositories.UserRepository;
import com.enhance.images.xlib.ImageData;
import com.enhance.images.xlib.JavaClassLoader;
import com.enhance.images.xlib.UsefulFunction;
import org.opencv.core.Mat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.*;

@Service
public class ImageServiceImpl implements ImageService {
    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Image> imageGet() {

        List<Image> images = imageRepository.findAll();
        List<Image> toret = new LinkedList<Image>();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        for (Image image : images) {
            if (image.getUser().getUsername().equals(auth.getName())) {
                toret.add(image);
            }
        }
        return toret;
    }

    @Override
    public ImageData imagePostFromUrl(Image image, String imageUrl) throws NoImageDataException, 
                                    NoFiltersProvidedException, NoImageExtException, IOException {

        final String pack = "com.enhance.images.filters.";
        JavaClassLoader javaClassLoader = new JavaClassLoader();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        ArrayList<String> filters = image.getFilters();
        String ext;
        BufferedImage imageData;
        Mat matImage;

        if (imageUrl == null) {
            throw new NoImageDataException();
        }

        if (filters == null) {
            throw new NoFiltersProvidedException();
        }

        ext = UsefulFunction.getFileExt(imageUrl);

        if (ext == null) {
            throw new NoImageExtException();
        }

        image.setImageExt(ext);
        image.setImageData(null);

        imageData = UsefulFunction.readFromUrl(imageUrl);
        matImage = UsefulFunction.bufferedImage2Mat(imageData, UsefulFunction.getFileFormat(imageUrl));


        ListFilters listFilters = new ListFilters();

        for (String filter : filters) {
            String className = listFilters.convertFilterToClass(filter);
            if (className == null)
                throw new NoClassDefFoundError();
            Object result = javaClassLoader.invokeClassMethod(pack + className, "applyFilter", matImage);
            matImage = (Mat) result;
        }

        String path = UsefulFunction.saveImage(matImage, ext, auth.getName());

        image.setImageUrl(path);
        image.setImageData(null); /*don't want to save image data in database*/

        long id = save(image);


        byte[] bytes1 = UsefulFunction.matToByte(matImage, ext);
        String base64Image = UsefulFunction.byteToBase64(bytes1);
        ImageData data = new ImageData(base64Image);
        data.setId(id);
        return data;

    }

    @Override
    public ImageData imagePost(Image image) throws NoImageDataException, NoFiltersProvidedException, NoImageExtException {

        final String pack = "com.enhance.images.filters.";
        JavaClassLoader javaClassLoader = new JavaClassLoader();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        byte[] bytes;
        ImageData data;
        String bas64Image;
        String base64Image;

        bas64Image = image.getImageData();
        ArrayList<String> filters = image.getFilters();
        String ext = image.getImageExt();

        if (bas64Image == null) {
            throw new NoImageDataException();
        }

        if (filters == null) {
            throw new NoFiltersProvidedException();
        }

        if (ext == null) {
            throw new NoImageExtException();
        }


        bytes = UsefulFunction.base64ToByte(bas64Image);

        ListFilters listFilters = new ListFilters();
        Mat matImage = UsefulFunction.byteToMat(bytes);

        for (String filter : filters) {
            String className = listFilters.convertFilterToClass(filter);
            if (className == null)
                throw new NoClassDefFoundError();
            Object result = javaClassLoader.invokeClassMethod(pack + className, "applyFilter", matImage);
            matImage = (Mat) result;
        }

        String path = UsefulFunction.saveImage(matImage, ext, auth.getName());

        image.setImageUrl(path);
        image.setImageData(null); /* don't want to save image data in database */

        long id = save(image);


        bytes = UsefulFunction.matToByte(matImage, ext);
        base64Image = UsefulFunction.byteToBase64(bytes);
        data = new ImageData(base64Image);
        data.setId(id);
        return data;
    }

    @Override
    public Image imagePut(Image newImage, long id) throws IOException {

        Optional<Image> image = imageRepository.findById(id);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
       
        if (image.get().getUser().getUsername().equals(auth.getName()) ||
                isAdmin()) {
            newImage.setId(image.get().getId());
            save(newImage);
            return imageRepository.findById(id).get();
        } else {
            throw new IOException();
        }
    }

    @Override
    public Image imagePatch(Image newImage, long id) throws IOException {

        Optional<Image> image = imageRepository.findById(id);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (image.get().getUser().getUsername().equals(auth.getName()) ||
                isAdmin()) {

            if (newImage.getImageData() != null &&
                    newImage.getImageData().equals("")) {
                image.get().setImageData(null);
            } else if (newImage.getImageData() != null &&
                    !newImage.getImageData().isEmpty()) {
                image.get().setImageData(newImage.getImageData());
            }

            if (newImage.getImageUrl() != null &&
                    newImage.getImageUrl().equals("")) {
                image.get().setImageUrl(null);
            } else if (newImage.getImageUrl() != null &&
                    !newImage.getImageUrl().isEmpty()) {
                image.get().setImageUrl(newImage.getImageUrl());
            }

            if (newImage.getDescription() != null &&
                    newImage.getDescription().equals("")) {
                image.get().setDescription(null);
            } else if (newImage.getDescription() != null &&
                    !newImage.getDescription().isEmpty()) {
                image.get().setDescription(newImage.getDescription());
            }

            if (newImage.getFilters() != null)
                image.get().setFilters(newImage.getFilters());

            save(image.get());
            return imageRepository.findById(id).get();
        } else {
            throw new IOException();
        }
    }

    @Override
    public long imageDelete(long id) throws IOException {

        Optional<Image> image = imageRepository.findById(id);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (image.get().getUser().getUsername().equals(auth.getName()) ||
                isAdmin()) {

            String path = image.get().getImageUrl();
            UsefulFunction.deleteImage(path);
            imageRepository.deleteById(id);
            return id;

        } else {
            throw new IOException();
        }
    }

    @Override
    public String imageGet(long id) throws IOException {
        Optional<Image> images = imageRepository.findById(id);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!images.isEmpty() && (images.get().getUser().getUsername().equals(auth.getName()) ||
             isAdmin())) {
            String path = images.get().getImageUrl();
            BufferedImage openImage = UsefulFunction.readImage(path);
            String format = images.get().getImageExt().substring(1);
            byte[] imageByte = UsefulFunction.bufferedImageToByte(openImage, format);
            String strImage = UsefulFunction.byteToBase64(imageByte);

            return strImage;
        } else {
            throw new AccessDeniedException("You are not authorized");
        }
    }

    @Override
    public long save(Image image) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        image.setUser(userRepository.findByUsername(auth.getName()));

      return imageRepository.save(image).getId();
    }

    private  boolean isAdmin(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getAuthorities().toString().contains("ROLE_ADMIN");
    }
}
