package com.enhance.images;

import com.enhance.images.controllers.FiltersController;
import com.enhance.images.controllers.ImageController;
import com.enhance.images.controllers.UsersController;
import com.enhance.images.models.Image;
import com.enhance.images.models.User;
import com.enhance.images.repositories.ImageRepository;
import com.enhance.images.repositories.UserRepository;
import com.enhance.images.security.MyBasicSecurity;
import com.enhance.images.service.FilterService;
import com.enhance.images.service.ImageService;
import com.enhance.images.service.UserService;
import com.enhance.images.xlib.ImageData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import javax.imageio.ImageIO;
import javax.sql.DataSource;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {UsersController.class, FiltersController.class, ImageController.class}, secure = false)
public class ControllersTests {

	private MockMvc mockMvc;

	// @MockBean
	// private DataSource dataSource;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private UsersController usersController;

	@Autowired
	private ImageController imageController;

	@Autowired
	private FiltersController filtersController;

	@MockBean
	private ImageService imageServiceMock;

	@MockBean
	private UserService userServiceMock;

	@MockBean
	private FilterService filterServiceMock;
	


	@Before
	public void setUp() {
		this.mockMvc = webAppContextSetup(webApplicationContext)
//				.apply(springSecurity())
				.build();
	}


	@Test
	public  void  testSuccessfulCreationOfController(){
		assertThat(filtersController).isNotNull();
		assertThat(imageController).isNotNull();
		assertThat(usersController).isNotNull();
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	public void testCreateUser() throws Exception {
		User user = new User();
		user.setUsername("john");
		user.setPassword("john");
		user.setEmail("john@company.com");
		user.setId(12345L);
		when(userServiceMock.createUser(any(User.class))).thenReturn(12345L);
		mockMvc.perform(post("/api/v1/register")
				.with((httpBasic("user","password")))
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(asJsonString(user))
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.message").value("User{id=12345, email='john@company.com'," +
						" username='john', firstName='null', " +
						"lastName='null', dob='null'} has been created."));
	}


//	@Test
//	@WithMockUser(roles = "USER")
//	public void testCreateUserFail() throws Exception {
//		User user = new User();
//		user.setUsername("john");
//		user.setPassword("john");
//		user.setEmail("john@company.com");
//		user.setId(12345L);
//		when(userServiceMock.createUser(any(User.class))).thenReturn(12345L);
//		mockMvc.perform(post("/api/v1/register")
//				.with((httpBasic("user","password")))
//				.contentType(MediaType.APPLICATION_JSON)
//				.content(asJsonString(user))
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isForbidden());
//	}


	@Test
	public  void  testGetFilters() throws Exception {
		HashMap<String, String> filters = new HashMap<String, String>();
			 filters.put("black_and_white", "transform an image to Black and white");

		when(filterServiceMock.getFiltersList()).thenReturn(filters);
		mockMvc.perform(get("/api/v1/filters"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

	}


	@Test
	@WithMockUser(roles = "USER")
	public void testImagePost() throws  Exception {
		Image image = new Image();
		ArrayList<String> filters = new ArrayList<String>();

		filters.add("grayify");

		image.setImageExt(".jpg");
		image.setFilters(filters);
		image.setImageData(getTestImage());
		ImageData result = new ImageData(getTestImage());
		result.setId(1);

		when(imageServiceMock.imagePost(any(Image.class))).thenReturn(result);

		mockMvc.perform(post("/api/v1/images")
				.with((httpBasic("user","password")))
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(image))
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.imageData").value(getTestImage()));
	}


	@Test
	@WithMockUser(roles = "USER")
	public void testImageGet() throws  Exception {
		Image image = new Image();
		List<Image> images = new ArrayList<>();
		ArrayList<String> filters = new ArrayList<String>();

		filters.add("grayify");

		image.setId(1);
		image.setImageExt(".jpg");
		image.setFilters(filters);
		image.setImageData(getTestImage());

		images.add(image);

		when(imageServiceMock.imageGet()).thenReturn(images);

		mockMvc.perform(get("/api/v1/images")
				.with((httpBasic("user","password")))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.[0].id").value(1));
	}


	@Test
	@WithMockUser(roles = "USER")
	public void testImageGetId() throws  Exception {
		Image image = new Image();
		ArrayList<String> filters = new ArrayList<String>();

		filters.add("grayify");

		image.setImageExt(".jpg");
		image.setFilters(filters);
		image.setImageData(getTestImage());
		ImageData result = new ImageData(getTestImage());
		result.setId(1);

		when(imageServiceMock.imageGet(any(Long.class))).thenReturn(getTestImage());

		mockMvc.perform(get("/api/v1/images/{id}", 1)
				.with((httpBasic("user","password")))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(mvcResult -> getTestImage());
	}
	


	@WithMockUser(roles = "USER")
	private String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	private String getTestImage() throws IOException {
		File file = new File("." + File.separator+"testdata"+File.separator+"penguin.jpg");
		BufferedImage bufferedImage =  ImageIO.read(file);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "jpg", bos);
		byte[] imageBytes = bos.toByteArray();
		return   Base64.getEncoder().encodeToString(imageBytes);

	}
}
